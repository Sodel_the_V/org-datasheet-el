(require 'org)

(defgroup org-datasheet nil "")             ;TODO!

(defcustom org-datasheet-insertable-null-string ""
  "String used when a property is nil."
  :group 'org-datasheet
  :type 'string)

(defcustom org-datasheet-header nil
  "A list of strings to insert before the datasheet template."
  :group 'org-datasheet
  :type `(choice (const :tag "None" nil)
                 (const :tag "Maintainer-Suggested Header" (list "#+LATEX: \\begin{alltt}"))
                 (repeat :tag "Custom Lines" string)))

(defcustom org-datasheet-footer ""
  "A list of strings to append after the end of the datasheet template."
  :group 'org-datasheet
  :type `(choice (const :tag "None" nil)
                 (const :tag "Maintainer-Suggested Footer" (list "#+LATEX: \\end{alltt}"))
                 (repeat :tag "Custom Lines" string)))

(defcustom org-datasheet-auto-import-latex-alltt t
  "If non-nil, loading org-datasheet will add alltt to `org-latex-packages-alist'.

This is so the suggested header & footer values, which use alltt,
work \"out of the box\"."
  :group 'org-datasheet
  :type 'bool)

(when org-datasheet-auto-import-latex-alltt
  (add-to-list 'org-latex-packages-alist '("" "alltt")))

(defun org-datasheet--get-template-str (template-name)
  (s-join "\n"
          (-flatten
           (list
            org-datasheet-header
            (save-excursion
              (org-babel-goto-named-src-block
               (if (symbolp template-name)
                   (symbol-name template-name)
                 template-name))
              (org-trim (org-element-property :value (org-element-at-point)) t))
            org-datasheet-footer))))

(defun org-datasheet--find-next-property-names (str)
  (when (ignore-errors
            (string-match "\\B\\(:\\(\\S-+\\):OR\\)?:\\(\\S-+\\):"
                          str
                          (match-beginning 0)))
    (save-match-data
      (mapcar (lambda (s) (string-trim s ":" ":"))
              (split-string (match-string 0 str) ":OR:" t)))))

(defun org-datasheet-exists-in-entry? (id)
  (save-restriction
    (widen)
    (save-excursion
      (org-id-goto id)
      (s-matches? "^#\\+BEGIN: datasheet"
                  (replace-regexp-in-string "^\\*\\(.\\|\n\\)*" ""
                                            (substring-no-properties (org-get-entry)))))))

(defun org-datasheet--instantiate-template-str (template-str)
  (string-match "^" template-str)
  (while
      (let* ((propnames (org-datasheet--find-next-property-names template-str))
             propname propval)
        (when propnames
          (save-match-data
            (setq propname (car propnames))
            (while (and propname (null propval))
              (setq propval (org-entry-get (point) propname)
                    propname (car propnames)
                    propnames (cdr propnames))))
          (when (null propval)
            (setq propval org-datasheet-insertable-null-string))
          (setq template-str (replace-match propval t t template-str)))))
  template-str)

(defun org-dblock-write:datasheet (params)
  (let* ((template-name (plist-get params :template))
         (template-str (or (org-datasheet--get-template-str template-name)
                           (user-error (format "No template found: %s" template-name))))
         (str (org-datasheet--instantiate-template-str template-str)))
    (insert str)
    (previous-line)
    (when (org-at-table-p)
      (org-table-align))))

(provide 'org-datasheet)
